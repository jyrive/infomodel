
# This should check modbus yaml files with set of rules, like
# 1. modbus protocol spec
# 2. open api spec
# 3. vaisala internal rules
# 4. modbus registers won't overlap
# 5. register access is between 0...65535 (??)
# 6. some other???
#
# The purpose of the validator is that, whoever writes a new yaml file, will notice 
# immediately if there is something wrong and can fix it
#
# The validator can be also used for PR's already to check the validity

