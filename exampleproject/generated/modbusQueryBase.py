
# Base class for generated code i.e. modbus queries
class ModbusQueryBase:
    def __init__(self, startAddress):
        self.startAddress = startAddress
    
    def read(self, startAddress, responseSize):
        # return default error (0x84) with exception code not supported 0x01 
        return 0x84, 1, [0x01] # returnCode, responseSize, array[responseSize]
    
    def write(self):
        pass
