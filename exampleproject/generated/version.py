
from generated.modbusQueryBase import ModbusQueryBase

class software_version(ModbusQueryBase):
    def __init__(self):
        super(software_version, self).__init__(1)
    
    def read(self, startAddress, responseSize):
        assert(startAddress==1)
        assert(responseSize==3)
        return 4, 3, [1,2,33]
    
    def write(self):
        pass
