#!/usr/bin/env python3
import glob
import yaml 
import ntpath
import os

def getFilename(path):
    head, tail = ntpath.split(path)
    filename=  tail or ntpath.basename(head)
    return os.path.splitext(filename)[0]

templateString = "\n\
from generated.modbusQueryBase import ModbusQueryBase\n\
\n\
class {class}(ModbusQueryBase):\n\
    def __init__(self):\n\
        super({class}, self).__init__({startAddress})\n\
    \n\
    def read(self, startAddress, responseSize):\n\
        assert(startAddress=={startAddress})\n\
        assert(responseSize=={responseSize})\n\
        return {functionCode}, {responseSize}, [fill registers to this array]\n\
    \n\
    def write(self):\n\
        pass\n\
"

yaml_files  = glob.glob("../api/modbus/*.yaml")

data = None
for yaml_file in yaml_files:
    print(yaml_file)
    filename = getFilename(yaml_file)
    print(filename)
    with open(yaml_file) as f:
        data = yaml.safe_load(f)

        # basic check having valid first level elements in yaml files
        if 'info' not in data.keys():
            print("'info' missing from yaml file")
            exit()

        if 'modbus-queries' not in data.keys():
            print("'modbus-queries' missing from yaml file")
            exit()

        queries = data['modbus-queries'].keys()

        for query in queries:
            funcCodes = data['modbus-queries'][query].keys()

            for funcCode in funcCodes:      
                print(funcCode + ', ' + query)
                print(type(funcCode))

                if funcCode == 'read-input-registers':
                    d = {}
                    d['class']        = query
                    d['functionCode'] = data['modbus-queries'][query][funcCode]['x-modbus-function-code']
                    d['responseSize'] = data['modbus-queries'][query][funcCode]['x-modbus-quantity-of-input-registers']
                    d['startAddress'] = data['modbus-queries'][query][funcCode]['x-modbus-starting-address']
                    print(d)
                    with open("generated/"+filename+".py", 'w') as f:
                        f.write(templateString.format(**d))




