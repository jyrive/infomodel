#!/usr/bin/env python
import socket
from modbus import *

def server_program(modbus):
    host = socket.gethostname()
    port = 5000

    server_socket = socket.socket()
    # look closely. The bind() function takes tuple as argument
    server_socket.bind((host, port))

    # configure how many client the server can listen simultaneously
    server_socket.listen(2)
    conn, address = server_socket.accept()
    print("Connection from: " + str(address))
    while True:
        # receive data stream. it won't accept data packet greater than 1024 bytes
        data = conn.recv(1024).decode()
        if not data:
            break # break if no data
        print("server recv: " + str(data))
        print(type(data))
        print(data)
        #modbus.dispatch(data[0])
        #conn.send(data.encode())  # send data to the client
        responselist = [1,2,3]
        conn.send(', '.join(str(e) for e in responselist).encode())  # send data to the client

    conn.close()  # close the connection


if __name__ == '__main__':
    # create the fake modbus simulator and run the server
    server_program(ModbusFake())