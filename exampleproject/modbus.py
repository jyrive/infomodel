

from generated import *

# simple hash table with chaining to store modbus commands
class HashTable:
    def __init__(self, size=10):
        self.hash_table = [[] for _ in range(size)]

    def insert(self, key, value):
        hash_key = hash(key) % len(self.hash_table)
        key_exists = False
        bucket = self.hash_table[hash_key]    
        for i, kv in enumerate(bucket):
            k, v = kv
            if key == k:
                key_exists = True 
                break
        if key_exists:
            bucket[i] = ((key, value))
        else:
            bucket.append((key, value))

    def search(self, key):
        hash_key = hash(key) % len(self.hash_table)    
        bucket = self.hash_table[hash_key]
        for i, kv in enumerate(bucket):
            k, v = kv
            if key == k:
                return v
        return None


# fake modbus for example project purposes
class ModbusFake:

    def __init__(self, tableSize=100):
        self.commands = HashTable(tableSize)

        # create and add the generated modbus queries here (need to be done manually)
        self.insert(version.software_version())

    def insert(self, command):
        self.commands.insert(command.startAddress, command)

    def dispatch(self, funcCode, address):
        command = self.commands.search(address)
        if command != None:
            if funcCode == 4:
                return command.read()
            else:
                return None


